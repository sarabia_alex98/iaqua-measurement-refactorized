package com.example.measures.repository;

import com.example.measures.model.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;

@Transactional
public interface MeasurementDAO extends JpaRepository<Measurement, Long> {
    @Modifying
    @Query(value = "update Measurement m set m.mseJustification='caca' where m.mseId= :idMedicion")
    void changeJustification(@Param("idMedicion") Long idMedicion);
    @Modifying
    @Query(value = "update Measurement m set m.mseStatus='ALTERADO' where m.mseId= :idMedicion")
    void changeState(@Param("idMedicion") Long idMedicion);

    @Query(value = "select m from Measurement m where m.mseStatus= 'OK'")
    Collection<Measurement> getAllMeasurementsStatusOk();

    @Query(value = "select m from Measurement m where m.mseUserWhoSendId= :idOperator")
    Collection<Measurement> getMeasurementsByOperator(@Param("idOperator") Long idOperator);

    @Query(value = "select m from Measurement m where m.msePlantWhereSendId= :idPlant")
    Collection<Measurement> getMeasurementsByPlant(@Param("idPlant") Long idPlant);


    @Query(value = "select m from Measurement m where m.mseStatus= 'ALTERADO'")
    Collection<Measurement> getAllMeasurementsStatusALTERADO();


}
