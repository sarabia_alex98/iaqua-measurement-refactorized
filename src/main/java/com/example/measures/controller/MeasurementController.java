package com.example.measures.controller;

import com.example.measures.model.Measurement;
import com.example.measures.services.MeasurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods={RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/measurement")
public class MeasurementController {
    @Autowired
    private MeasurementService measurementService;

    @GetMapping("/all")
    public List<Measurement> getAllMeasurements(){
        return measurementService.getAllMeasurementsOfEntireSystem();
    }
    @GetMapping("/{idMeasure}")
    public Measurement getMeasurementById(@PathVariable Long idMeasure){
        return measurementService.getMeasurementById(idMeasure);
    }
    @GetMapping("/status/OK")
    public List<Measurement> getMeasurementByStatus(){
        return measurementService.getAllMeasurementsStatusOk();
    }
    @GetMapping("/status/ALTERADO")
    public List<Measurement> getAllMeasurementsStatusALTERADO(){
        return measurementService.getAllMeasurementsStatusALTERADO();
    }

    @GetMapping("/operator/{idOperator}")
    public List<Measurement> getMeasurementByOperator(@PathVariable("idOperator") Long idOperator){
        return measurementService.getMeasurementsByOperator(idOperator);
    }
    @GetMapping("/plant/{idPlant}")
    public List<Measurement> getMeasurementByPlant(@PathVariable("idPlant") Long idPlant){
        return measurementService.getMeasurementsByPlant(idPlant);
    }

    @PostMapping("/add")
    public Measurement addMeasurement(@RequestBody Measurement newMeasurement) throws IOException {
        return measurementService.addMeasurement(newMeasurement);
    }
}