package com.example.measures.apicalls;

import lombok.Getter;

import java.io.IOException;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.Scanner;

@Getter
public class PlantsCalls {
    private boolean plaChlorineExist;
    private boolean plaFluorineExist;
    private boolean plaPhExist;
    private boolean plaSedimentExist;
    private boolean plaMagnesiumExist;
    private boolean plaIronExist;

    private double chlorineLimitInf;
    private double chlorineLimitSup;
    private double fluorineLimitInf;
    private double fluorineLimitSup;
    private double phLimitInf;
    private double phLimitSup;
    private double sedimentLimitInf;
    private double sedimentLimitSup;
    private double magnesiumLimitInf;
    private double magnesiumLimitSup;
    private double ironLimitInf;
    private double ironLimitSup;

    public PlantsCalls(long msePlantWhereSendId) throws IOException {
        String urlString="https://apppl-medicionagua.smartaraucania.org/plant/";
        String plantId=String.valueOf(msePlantWhereSendId);

        URL url= new URL(urlString+plantId);
        HttpURLConnection con=(HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();
        StringBuilder jsonputo=new StringBuilder();
        int response=con.getResponseCode();
        if(response!=200){
            System.out.println("no conecta con la API");
        }else{
            Scanner scanner=new Scanner(url.openStream());
            while(scanner.hasNext()){
                jsonputo.append(scanner.nextLine());
            }
            scanner.close();
        }

        //En este punto ya recibimos la medición

        //tomar el StringBuild y separarlo
        String [] plantAtributes=jsonputo.toString().split(",");

        plaChlorineExist = chemicalExistence(plantAtributes, "chlorine");
        plaFluorineExist = chemicalExistence(plantAtributes, "fluorine");
        plaPhExist = chemicalExistence(plantAtributes, "ph");
        plaSedimentExist = chemicalExistence(plantAtributes, "sediment");
        plaMagnesiumExist = chemicalExistence(plantAtributes, "megnesium");
        plaIronExist = chemicalExistence(plantAtributes, "iron");



        chlorineLimitInf=obteinLimits("chlorine","inf", plantAtributes);
        chlorineLimitSup=obteinLimits("chlorine","sup", plantAtributes);

        fluorineLimitInf=obteinLimits("fluorine","inf", plantAtributes);
        fluorineLimitSup=obteinLimits("fluorine","sup", plantAtributes);

        phLimitInf=obteinLimits("ph","inf", plantAtributes);
        phLimitSup=obteinLimits("ph","sup", plantAtributes);

        sedimentLimitInf=obteinLimits("sediment","inf", plantAtributes);
        sedimentLimitSup=obteinLimits("sediment","sup", plantAtributes);

        magnesiumLimitInf=obteinLimits("magnesium","inf", plantAtributes);
        magnesiumLimitSup=obteinLimits("magnesium","sup", plantAtributes);

        ironLimitInf=obteinLimits("iron","inf", plantAtributes);
        ironLimitSup=obteinLimits("iron","sup", plantAtributes);
    }

    public boolean[] chemicalStates(double chlorineAmount, double fluorineAmount, double phAmount, double sedimentAmount, double magnesiumAmount, double ironAmount) throws IOException {

        boolean[]states={true,true,true,true,true,true};//0: Cloro; 1:Fluor; 2:Ph; 3: Turbiedad; 4: Magnesio; 5: Fierro
        if(plaChlorineExist) {
            if (!restrictionEvaluation(chlorineAmount, chlorineLimitInf, chlorineLimitSup)) {
                states[0] = false;
            }
        }
        if(plaFluorineExist) {
            if (!restrictionEvaluation(fluorineAmount, fluorineLimitInf, fluorineLimitSup)) {
                states[1] = false;
            }
        }
        if(plaPhExist) {
            if (!restrictionEvaluation(phAmount, phLimitInf, phLimitSup)) {
                states[2] = false;
            }
        }
        if(plaSedimentExist) {
            if (!restrictionEvaluation(sedimentAmount, sedimentLimitInf, sedimentLimitSup)) {
                states[3] = false;
            }
        }
        if(plaMagnesiumExist) {
            if (!restrictionEvaluation(magnesiumAmount, magnesiumLimitInf, magnesiumLimitSup)) {
                states[4] = false;
            }
        }
        if(plaIronExist) {
            if (!restrictionEvaluation(ironAmount, ironLimitInf, ironLimitSup)) {
                states[5] = false;
            }
        }
        return states;
    }
    private boolean restrictionEvaluation(double chemicalAmount, double chemicalLimitInf, double chemicalLimitSup) {
        if((chemicalAmount>=chemicalLimitInf && chemicalAmount<=chemicalLimitSup) && differentToZero(chemicalLimitInf, chemicalLimitSup)){ //si los limites son '0' significa que esos químicos no aplican a la planta en cuestión
            return true;
        }else{
            return false;
        }
    }

    private boolean differentToZero(double chemicalLimitInf, double chemicalLimitSup) {
        if(chemicalLimitInf!=0 && chemicalLimitSup!=0){
            return true;
        }else{
            return false;
        }
    }

    private static double obteinLimits(String chemical, String limit, String[] plantAtributes) {
        if(chemical.equals("chlorine")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaChlorineLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaChlorineLimitSup")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
            }
        }else if(chemical.equals("fluorine")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaFluorineLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaFluorineLimitSup")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
            }
        }else if(chemical.equals("ph")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaPhLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaPhLimitSup")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
            }
        }else if(chemical.equals("sediment")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaSedimentLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaSedimentLimitSup")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
            }
        }else if(chemical.equals("magnesium")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaMagnesiumLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaMagnesiumLimitSup")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
            }
        }else if(chemical.equals("iron")){
            switch(limit){
                case "inf":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaIronLimitInf")){
                            String []splitValue=plantAtributes[i].split(":");
                            return Double.parseDouble(splitValue[1]);
                        }
                    }
                    break;
                case "sup":
                    for (int i = 0; i < plantAtributes.length; i++) {
                        if(plantAtributes[i].contains("plaIronLimitSup")){
                            String []splitValue=plantAtributes[i].split(":"); //  devuelve 0.0}
                            String []splitValueFinal=splitValue[1].split("}"); //aca raro
                            return Double.parseDouble(splitValueFinal[0]);
                        }
                    }
                    break;
            }
        }
        return 0;
    }

    private static boolean chemicalExistence(String[] plantAtributes, String chemical) {
        boolean flag=false;
        switch(chemical){
            case "chlorine":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaChlorineExist\":true")){
                        flag=true;
                    }
                }
                break;
            case "fluorine":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaFluorineExist\":true")){
                        flag=true;
                    }
                }
                break;
            case "ph":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaPhExist\":true")){
                        flag=true;
                    }
                }
                break;
            case "sediment":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaSedimentExist\":true")){
                        flag=true;
                    }
                }
                break;
            case "megnesium":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaMagnesiumExist\":true")){
                        flag=true;
                    }
                }
                break;
            case "iron":
                for (int i = 0; i < plantAtributes.length; i++) {
                    if(plantAtributes[i].contains("\"plaIronExist\":true")){
                        flag=true;
                    }
                }
                break;
        }
        return flag;
    }

}


