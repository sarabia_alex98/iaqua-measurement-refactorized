package com.example.measures.model;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "Measurement")
public class Measurement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long mseId;

    @Column(name = "mseDateAndHour",nullable = true)
    private String mseDateAndHour;
    @Column(name = "mseObservation",nullable = true)
    private String mseObservation;
    @Column(name = "msePhotography",nullable = true)
    private String msePhotography;
    @Column(name = "mseJustification",nullable = true)
    private String mseJustification;
    @Column(name = "mseGeoreference",nullable = true)
    private String mseGeoreference;

    @Column(name = "mseStatus",nullable = true)
    private String mseStatus;

    @Column(name = "mseUserWhoSentId",nullable = true)
    private long mseUserWhoSendId;
    @Column(name = "msePlantWhereSentId",nullable = true)
    private long msePlantWhereSendId;

    @Column(name = "mseChlorineAmount",nullable = true)
    private String mseChlorineAmount;
    @Column(name = "mseFluorineAmount",nullable = true)
    private String mseFluorineAmount;
    @Column(name = "msePhAmount",nullable = true)
    private String msePhAmount;
    @Column(name = "mseSedimentAmount",nullable = true)
    private String mseSedimentAmount;
    @Column(name = "mseMagnesiumAmount",nullable = true)
    private String mseMagnesiumAmount;
    @Column(name = "mseIronAmount",nullable = true)
    private String mseIronAmount;

    @Column(name = "mseChlorineIsOk",nullable = true)
    private boolean mseChlorineIsOk;
    @Column(name = "mseFluorineIsOk",nullable = true)
    private boolean mseFluorineIsOk;
    @Column(name = "msePhIsOk",nullable = true)
    private boolean msePhIsOk;
    @Column(name = "mseSedimentIsOk",nullable = true)
    private boolean mseSedimentIsOk;
    @Column(name = "mseMagnesiumIsOk",nullable = true)
    private boolean mseMagnesiumIsOk;
    @Column(name = "mseIronIsOk",nullable = true)
    private boolean mseIronIsOk;

    //cloro
    @Column(name = "mseChlorineCurrentMinLimit",nullable = true)
    private double mseChlorineCurrentMinLimit;
    @Column(name = "mseChlorineCurrentMaxLimit",nullable = true)
    private double mseChlorineCurrentMaxLimit;
    //fluor
    @Column(name = "mseFluorineCurrentMinLimit",nullable = true)
    private double mseFluorineCurrentMinLimit;
    @Column(name = "mseFluorineCurrentMaxLimit",nullable = true)
    private double mseFluorineCurrentMaxLimit;

    //ph
    @Column(name = "msePhCurrentMinLimit",nullable = true)
    private double msePhCurrentMinLimit;
    @Column(name = "msePhCurrentMaxLimit",nullable = true)
    private double msePhCurrentMaxLimit;

    //turbiedad
    @Column(name = "mseSedimentCurrentMinLimit",nullable = true)
    private double mseSedimentCurrentMinLimit;
    @Column(name = "mseSedimentCurrentMaxLimit",nullable = true)
    private double mseSedimentCurrentMaxLimit;

    //magnesio
    @Column(name = "mseMagnesiumCurrentMinLimit",nullable = true)
    private double mseMagnesiumCurrentMinLimit;
    @Column(name = "mseMagnesiumCurrentMaxLimit",nullable = true)
    private double mseMagnesiumCurrentMaxLimit;

    //fierro
    @Column(name = "mseIronCurrentMinLimit",nullable = true)
    private double mseIronCurrentMinLimit;
    @Column(name = "mseIronCurrentMaxLimit",nullable = true)
    private double mseIronCurrentMaxLimit;
}
