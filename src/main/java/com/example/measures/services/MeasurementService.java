package com.example.measures.services;


import com.example.measures.apicalls.PlantsCalls;
import com.example.measures.model.Measurement;
import com.example.measures.repository.MeasurementDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;

@Service
public class MeasurementService {

    @Autowired
    private MeasurementDAO measurementDAO;

    //post
    public Measurement addMeasurement(Measurement newMeasurement) throws IOException {
        PlantsCalls plantsCalls=new PlantsCalls(newMeasurement.getMsePlantWhereSendId());

        double chlorineAmount=Double.parseDouble(newMeasurement.getMseChlorineAmount());
        double fluorineAmount=Double.parseDouble(newMeasurement.getMseFluorineAmount());
        double phAmount=Double.parseDouble(newMeasurement.getMsePhAmount());
        double sedimentAmount=Double.parseDouble(newMeasurement.getMseSedimentAmount());
        double magnesiumAmount=Double.parseDouble(newMeasurement.getMseMagnesiumAmount());
        double ironAmount=Double.parseDouble(newMeasurement.getMseIronAmount());

        //cuando creemos una instancia de PlantsCalls vamos a pasarle por parámetro:
        //newMeasurement.getMsePlantWhereSendId(), chlorineAmount, fluorineAmount, phAmount, sedimentAmount, magnesiumAmount, ironAmount

        //0: Cloro; 1:Fluor; 2:Ph; 3: Turbiedad; 4: Magnesio; 5: Fierro
        boolean[]statesForChemical=plantsCalls.chemicalStates(chlorineAmount, fluorineAmount, phAmount, sedimentAmount, magnesiumAmount, ironAmount);

        newMeasurement.setMseChlorineIsOk(statesForChemical[0]);
        newMeasurement.setMseFluorineIsOk(statesForChemical[1]);
        newMeasurement.setMsePhIsOk(statesForChemical[2]);
        newMeasurement.setMseSedimentIsOk(statesForChemical[3]);
        newMeasurement.setMseMagnesiumIsOk(statesForChemical[4]);
        newMeasurement.setMseIronIsOk(statesForChemical[5]);


        //definir currentMinLimit y currentMaxLimit
        //cloro
        newMeasurement.setMseChlorineCurrentMinLimit(plantsCalls.getChlorineLimitInf());
        newMeasurement.setMseChlorineCurrentMaxLimit(plantsCalls.getChlorineLimitSup());

        //fluor
        newMeasurement.setMseFluorineCurrentMinLimit(plantsCalls.getFluorineLimitInf());
        newMeasurement.setMseFluorineCurrentMaxLimit(plantsCalls.getFluorineLimitSup());

        //ph
        newMeasurement.setMsePhCurrentMinLimit(plantsCalls.getPhLimitInf());
        newMeasurement.setMsePhCurrentMaxLimit(plantsCalls.getPhLimitSup());

        //turbiedad
        newMeasurement.setMseSedimentCurrentMinLimit(plantsCalls.getSedimentLimitInf());
        newMeasurement.setMseSedimentCurrentMaxLimit(plantsCalls.getSedimentLimitSup());

        //magnesio
        newMeasurement.setMseMagnesiumCurrentMinLimit(plantsCalls.getMagnesiumLimitInf());
        newMeasurement.setMseMagnesiumCurrentMaxLimit(plantsCalls.getMagnesiumLimitSup());

        //fierro
        newMeasurement.setMseIronCurrentMinLimit(plantsCalls.getIronLimitInf());
        newMeasurement.setMseIronCurrentMaxLimit(plantsCalls.getIronLimitSup());

        //si o si el estado de la medicion
        boolean stateAlteredFound=false;
        for (int i = 0; i < statesForChemical.length; i++) {
            if(!statesForChemical[i]){
                stateAlteredFound=true;
            }
        }
        if(stateAlteredFound){
            newMeasurement.setMseStatus("ALTERADO");
        }else{
            newMeasurement.setMseStatus("OK");
        }

        return measurementDAO.save(newMeasurement);
    }
    //get
    public List<Measurement> getAllMeasurementsOfEntireSystem(){
        return measurementDAO.findAll();
    }
    public Measurement getMeasurementById(Long measurementId){
        if(measurementDAO.findById(measurementId).isPresent()){ //this just valide a not-null object 'Measurement', but 'findById' is still deprecated... can be replaced for something else?
            return measurementDAO.getReferenceById(measurementId);
        }else{
            return null; //can this throw an exception?
        }
    }
    public List<Measurement> getAllMeasurementsStatusOk(){
        return measurementDAO.getAllMeasurementsStatusOk().stream().toList();
    }
    public List<Measurement> getAllMeasurementsStatusALTERADO(){
        return measurementDAO.getAllMeasurementsStatusALTERADO().stream().toList();
    }
    public List<Measurement> getMeasurementsByOperator(Long idOperator){
        return measurementDAO.getMeasurementsByOperator(idOperator).stream().toList();
    }
    public List<Measurement> getMeasurementsByPlant(Long idPlant){
        return measurementDAO.getMeasurementsByPlant(idPlant).stream().toList();
    }

    //pruebas query
    public Measurement updateJustification(Long idMeasurement){
        measurementDAO.changeJustification(idMeasurement);
        return measurementDAO.getReferenceById(idMeasurement);
    }

    public Measurement changeState(Long idMeasurement){
        measurementDAO.changeState(idMeasurement);
        return measurementDAO.getReferenceById(idMeasurement);
    }
}