FROM openjdk:17-oracle
MAINTAINER iaqua.cl
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} measures-0.0.1.jar
ENTRYPOINT ["java","-jar","/measures-0.0.1.jar"]